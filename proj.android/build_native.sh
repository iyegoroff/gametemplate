# Copyright (C) 2013 Igor Yegorov
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

# check environment variables

if [[ -z "${ANDROID_NDK+aaa}" ]];then
	echo "please define ANDROID_NDK environment variable"
	exit 1
fi

if [[ -z "${COCOS2DX_HOME+aaa}" ]];then
	echo "please define COCOS2DX_HOME environment variable"
	exit 1
fi


APPNAME="GameTemplate"
NDK_PREFS="$WORKSPACE_FOLDER/.metadata/.plugins/org.eclipse.core.runtime/.settings/com.android.ide.eclipse.ndk.prefs"

# set NDK location (needed only for debugging)
python scripts/set_ndk_location.py $NDK_PREFS $ANDROID_NDK


# set environment
export USE_COCOSDENSHION=1
export USE_BOX2D=0
export USE_ASSETS_MANAGER=0
export USE_NETWORK=0
export USE_LOCAL_STORAGE=0
export USE_CCB_READER=0
export USE_GUI=0
export USE_PHYSICS_NODES=0
export USE_SPINE=0
export USE_EXTENSIONS=0 #don't work because of hardcoded chipmunk integration in armature

if [[ $USE_PHYSICS_NODES == 1 ]]; then
	export USE_BOX2D=1
fi

if [[ $USE_CCB_READER == 1 ]]; then
	export USE_COCOSDENSHION=1
fi

if [[ $USE_EXTENSIONS == 1 ]]; then
	export USE_COCOSDENSHION=1
	export USE_BOX2D=1
	export USE_ASSETS_MANAGER=0
	export USE_NETWORK=0
	export USE_LOCAL_STORAGE=0
	export USE_CCB_READER=0
	export USE_GUI=0
	export USE_PHYSICS_NODES=0
	export USE_SPINE=0
fi


# paths

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
COCOS2DX_ROOT=${COCOS2DX_HOME}
APP_ROOT="$DIR/.."
APP_ANDROID_ROOT="$DIR"

echo "ANDROID_NDK = $ANDROID_NDK"
echo "COCOS2DX_ROOT = $COCOS2DX_ROOT"
echo "APP_ROOT = $APP_ROOT"
echo "APP_ANDROID_ROOT = $APP_ANDROID_ROOT"

python scripts/build_extension/copy_scripts.py

# make sure assets is exist
if [[ -d "$APP_ANDROID_ROOT"/assets ]]; then
    rm -rf "$APP_ANDROID_ROOT"/assets
fi

mkdir "$APP_ANDROID_ROOT"/assets

# copy resources
for file in "$APP_ROOT"/Resources/*
do
if [[ -d "$file" ]]; then
    cp -rf "$file" "$APP_ANDROID_ROOT"/assets
fi

if [[ -f "$file" ]]; then
    cp "$file" "$APP_ANDROID_ROOT"/assets
fi
done


echo "USE_COCOSDENSHION = $USE_COCOSDENSHION"
echo "USE_BOX2D = $USE_BOX2D"
echo "USE_ASSETS_MANAGER = $USE_ASSETS_MANAGER"
echo "USE_NETWORK = $USE_NETWORK"
echo "USE_LOCAL_STORAGE = $USE_LOCAL_STORAGE"
echo "USE_CCB_READER = $USE_CCB_READER"
echo "USE_GUI = $USE_GUI"
echo "USE_PHYSICS_NODES = $USE_PHYSICS_NODES"
echo "USE_SPINE = $USE_SPINE"
echo "USE_EXTENSIONS = $USE_EXTENSIONS"
 

# run ndk-build
echo "Using prebuilt externals"
"$ANDROID_NDK"/ndk-build NDK_DEBUG=$NDK_DEBUG V=$NDK_DEBUG -C "$APP_ANDROID_ROOT" $* \
    "NDK_MODULE_PATH=${COCOS2DX_ROOT}:${COCOS2DX_ROOT}/cocos2dx/platform/third_party/android/prebuilt"


if [[ $REBUILD_COCOS2DX == 1 ]]; then
	python scripts/prebuild/copy_libs.py cocos2d armeabi armeabi-v7a
fi

if [[ $REBUILD_BOX2D == 1 ]] && [[ $USE_BOX2D == 1 ]]; then
	python scripts/prebuild/copy_libs.py box2d armeabi armeabi-v7a
fi

if [[ $REBUILD_ASSETS_MANAGER == 1 ]] && [[ $USE_ASSETS_MANAGER == 1 ]]; then
	python scripts/prebuild/copy_libs.py assets_manager armeabi armeabi-v7a
fi

if [[ $REBUILD_COCOSDENSHION == 1 ]] && [[ $USE_COCOSDENSHION == 1 ]]; then
	python scripts/prebuild/copy_libs.py cocosdenshion armeabi armeabi-v7a
fi

if [[ $REBUILD_LOCAL_STORAGE == 1 ]] && [[ $USE_LOCAL_STORAGE == 1 ]]; then
	python scripts/prebuild/copy_libs.py local_storage armeabi armeabi-v7a
fi

if [[ $REBUILD_CCB_READER == 1 ]] && [[ $USE_CCB_READER == 1 ]]; then
	python scripts/prebuild/copy_libs.py ccb_reader armeabi armeabi-v7a
fi

if [[ $REBUILD_GUI == 1 ]] && [[ $USE_GUI == 1 ]]; then
	python scripts/prebuild/copy_libs.py gui armeabi armeabi-v7a
fi

if [[ $REBUILD_NETWORK == 1 ]] && [[ $USE_NETWORK == 1 ]]; then
	python scripts/prebuild/copy_libs.py network armeabi armeabi-v7a
fi

if [[ $REBUILD_PHYSICS_NODES == 1 ]] && [[ $USE_PHYSICS_NODES == 1 ]]; then
	python scripts/prebuild/copy_libs.py physics_nodes armeabi armeabi-v7a
fi

if [[ $REBUILD_SPINE == 1 ]] && [[ $USE_SPINE == 1 ]]; then
	python scripts/prebuild/copy_libs.py spine armeabi armeabi-v7a
fi

if [[ $REBUILD_EXTENSIONS == 1 ]] && [[ $USE_EXTENSIONS == 1 ]]; then
	python scripts/prebuild/copy_libs.py extension armeabi armeabi-v7a
fi
