# Copyright (C) 2013 Igor Yegorov
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := cocos2dcpp_shared

LOCAL_MODULE_FILENAME := libcocos2dcpp

SRC_DIRS := $(shell find $(LOCAL_PATH)/../../Classes -type d -print)
SRCS     := $(foreach DIR, $(SRC_DIRS), $(wildcard $(DIR)/*.cpp) $(wildcard $(DIR)/*.c))

LOCAL_SRC_FILES := main/main.cpp \
                   $(subst jni/, , $(SRCS))

LOCAL_C_INCLUDES := $(SRC_DIRS)

LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static

ifeq ($(USE_COCOSDENSHION), 1)
	LOCAL_WHOLE_STATIC_LIBRARIES += cocosdenshion_static
endif

ifeq ($(USE_BOX2D), 1)
	LOCAL_WHOLE_STATIC_LIBRARIES += box2d_static
endif

ifeq ($(USE_ASSETS_MANAGER), 1)
LOCAL_WHOLE_STATIC_LIBRARIES += extension_assets_manager_static
endif

ifeq ($(USE_LOCAL_STORAGE), 1)
LOCAL_WHOLE_STATIC_LIBRARIES += extension_local_storage_static
endif

ifeq ($(USE_GUI), 1)
LOCAL_WHOLE_STATIC_LIBRARIES += extension_gui_static
endif

ifeq ($(USE_CCB_READER), 1)
LOCAL_WHOLE_STATIC_LIBRARIES += extension_ccb_reader_static
endif

ifeq ($(USE_NETWORK), 1)
LOCAL_WHOLE_STATIC_LIBRARIES += extension_network_static
endif

ifeq ($(USE_PHYSICS_NODES), 1)
LOCAL_WHOLE_STATIC_LIBRARIES += extension_physics_nodes_static
endif

ifeq ($(USE_SPINE), 1)
LOCAL_WHOLE_STATIC_LIBRARIES += extension_spine_static
endif

ifeq ($(USE_EXTENSIONS), 1)
LOCAL_WHOLE_STATIC_LIBRARIES += cocos_extension_static
endif

ifneq ($(REBUILD_COCOS2DX), 1)
	LOCAL_WHOLE_STATIC_LIBRARIES += cocos_libpng_static \
                                	cocos_jpeg_static \
                                	cocos_libxml2_static \
                                	cocos_libtiff_static \
                                	cocos_curl_static \
                                	cocos_libwebp_static
endif

ifeq ($(USE_ASSETS_MANAGER), 1)
ifneq ($(REBUILD_ASSETS_MANAGER), 1)
	LOCAL_WHOLE_STATIC_LIBRARIES += cocos_curl_static
endif
endif

ifeq ($(USE_NETWORK), 1)
ifneq ($(REBUILD_NETWORK), 1)
	LOCAL_WHOLE_STATIC_LIBRARIES += libwebsockets_static
endif
endif

ifeq ($(USE_EXTENSIONS), 1)
ifneq ($(REBUILD_ASSETS_MANAGER), 1)
	LOCAL_WHOLE_STATIC_LIBRARIES += cocos_curl_static
endif

ifneq ($(REBUILD_NETWORK), 1)
	LOCAL_WHOLE_STATIC_LIBRARIES += libwebsockets_static
endif
endif

include $(BUILD_SHARED_LIBRARY)

$(call import-add-path, $(COCOS2DX_HOME)/lib/android)

ifneq ($(REBUILD_COCOS2DX), 1)
$(call import-module, prebuilt/libcocos2d)
else
$(call import-module, cocos2dx)
endif

ifeq ($(USE_BOX2D), 1)
ifneq ($(REBUILD_BOX2D), 1)
$(call import-module, prebuilt/libbox2d)
else
$(call import-module, external/Box2D)
endif
endif

ifeq ($(USE_ASSETS_MANAGER), 1)
ifneq ($(REBUILD_ASSETS_MANAGER), 1)
$(call import-module, prebuilt/libassets_manager)
else
$(call import-module, extensions/AssetsManager)
endif
endif

ifeq ($(USE_COCOSDENSHION), 1)
ifneq ($(REBUILD_COCOSDENSHION), 1)
$(call import-module, prebuilt/libcocosdenshion)
else
$(call import-module, CocosDenshion/android)
endif
endif

ifeq ($(USE_LOCAL_STORAGE), 1)
ifneq ($(REBUILD_LOCAL_STORAGE), 1)
$(call import-module, prebuilt/liblocal_storage)
else
$(call import-module, extensions/LocalStorage)
endif
endif

ifeq ($(USE_CCB_READER), 1)
ifneq ($(REBUILD_CCB_READER), 1)
$(call import-module, prebuilt/libccb_reader)
else
$(call import-module, extensions/CCBreader)
endif
endif

ifeq ($(USE_GUI), 1)
ifneq ($(REBUILD_GUI), 1)
$(call import-module, prebuilt/libgui)
else
$(call import-module, extensions/GUI)
endif
endif

ifeq ($(USE_NETWORK), 1)
ifneq ($(REBUILD_NETWORK), 1)
$(call import-module, prebuilt/libnetwork)
else
$(call import-module, extensions/network)
endif
endif

ifeq ($(USE_PHYSICS_NODES), 1)
ifneq ($(REBUILD_PHYSICS_NODES), 1)
$(call import-module, prebuilt/libphysics_nodes)
else
$(call import-module, extensions/physics_nodes)
endif
endif

ifeq ($(USE_SPINE), 1)
ifneq ($(REBUILD_SPINE), 1)
$(call import-module, prebuilt/libspine)
else
$(call import-module, extensions/spine)
endif
endif

ifeq ($(USE_EXTENSIONS), 1)
ifneq ($(REBUILD_EXTENSIONS), 1)
$(call import-module, prebuilt/libextension)
else
$(call import-module, extensions)
endif
endif