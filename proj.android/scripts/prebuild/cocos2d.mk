# Copyright (C) 2013 Igor Yegorov
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE            := cocos2dx_static
LOCAL_MODULE_FILENAME   := cocos2dx
LOCAL_SRC_FILES         := $(TARGET_ARCH_ABI)/libcocos2d.a

LOCAL_EXPORT_C_INCLUDES := $(COCOS2DX_HOME)/cocos2dx/include \
                           $(COCOS2DX_HOME)/cocos2dx/platform/android \
						   $(COCOS2DX_HOME)/cocos2dx \
						   $(COCOS2DX_HOME)/cocos2dx/kazmath/include
						   
LOCAL_EXPORT_LDLIBS     := -llog \
                           -lz \
                           -lGLESv2 \
                           -landroid
                           
include $(PREBUILT_STATIC_LIBRARY)

$(call import-add-path, $(COCOS2DX_HOME)/cocos2dx/platform/third_party/android/prebuilt)

$(call import-module, libpng)
$(call import-module, libjpeg)
$(call import-module, libtiff)
$(call import-module, libwebp)