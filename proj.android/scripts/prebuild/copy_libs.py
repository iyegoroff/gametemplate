'''
Copyright (C) 2013 Igor Yegorov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

'''

'''
Created on Aug 18, 2012

usage: copy_libs.py <lib name> [armeabi] [armeabi-v7a]
'''

import shutil
import os
import sys

def copy_lib(lib, target):
    print 'copy_libs.py: Coping lib', lib, 'for target', target,
    srcdir = './obj/local/' + target
    dstdir = os.environ['COCOS2DX_HOME'] + '/lib/android/prebuilt/' + lib + '/' + target
    src = srcdir + '/' + lib + '.a'
    print 'from', src, 'to', dstdir
    if not os.path.isfile(src):
        print "copy_libs.py: WARNING! Can't find file", src
        return
    if not os.path.exists(dstdir): os.makedirs(dstdir)
    shutil.copyfile(src, dstdir + '/' + lib + '.a')
    

if 'armeabi' in sys.argv: copy_lib('lib' + sys.argv[1],'armeabi')
if 'armeabi-v7a' in sys.argv: copy_lib('lib' + sys.argv[1],'armeabi-v7a')
shutil.copyfile('./scripts/prebuild/' + sys.argv[1] + '.mk', os.environ['COCOS2DX_HOME'] + '/lib/android/prebuilt/lib' + sys.argv[1] + '/Android.mk')