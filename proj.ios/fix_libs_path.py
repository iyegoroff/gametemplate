'''
Copyright (C) 2013 Igor Yegorov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

'''

import os, re

proj_file = open("GameTemplate.xcodeproj/project.pbxproj",'r+')
proj_data = proj_file.read()

try:
    libs_path = os.environ['COCOS2DX_HOME']
except KeyError:
    raise Exception("Environment variable COCOS2DX_HOME undefined!")

new_data = re.sub(r'(?<=path = )("?.*?/cocos2dx.xcodeproj"?)', r'"' + libs_path + r'/cocos2dx/proj.ios/cocos2dx.xcodeproj"', proj_data)
new_data = re.sub(r'(?<=path = )("?.*?/external/Box2D"?)', r'"' + libs_path + r'/external/Box2D"', new_data)
new_data = re.sub(r'(?<=path = )("?.*?/CocosDenshion"?)', r'"' + libs_path + r'/CocosDenshion"', new_data)
new_data = re.sub(r'(?<=path = )("?.*?/extensions"?)', r'"' + libs_path + r'/extensions"', new_data)
new_data = re.sub(r'(?<=path = )("?.*?/external/libwebsockets"?)', r'"' + libs_path + r'/external/libwebsockets"', new_data)

if new_data != proj_data:
    proj_file.seek(0)
    proj_file.write(new_data)
    proj_file.truncate()
    proj_file.close()
