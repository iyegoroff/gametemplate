'''
Copyright (C) 2013 Igor Yegorov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

'''

# arg1 - compiler version: can be compiler-default or c++0x
# arg2 - library version: can be compiler-default or libc++

# "GameTemplate.xcodeproj/project.pbxproj"

import os, re, sys

def changeCppVersionInFile(pbxprojFile, compilerVersion, libraryVersion):
    proj_file = open(pbxprojFile,'r+')
    proj_data = proj_file.read()
    
    new_data = re.sub(r'(?<=CLANG_CXX_LANGUAGE_STANDARD = )(".*?";)', r'"' + compilerVersion + r'";', proj_data)
    new_data = re.sub(r'(?<=CLANG_CXX_LIBRARY = )(".*?";)', r'"' + libraryVersion + r'";', new_data)
    
    if new_data != proj_data:
        proj_file.seek(0)
        proj_file.write(new_data)
        proj_file.truncate()
        proj_file.close()

try:
    cocos2dx_path = os.environ['COCOS2DX_HOME'] + "/cocos2dx/proj.ios/cocos2dx.xcodeproj/project.pbxproj"
    changeCppVersionInFile(cocos2dx_path, sys.argv[1], sys.argv[2])
except KeyError:
    raise Exception("Environment variable COCOS2DX_HOME undefined!")

changeCppVersionInFile("GameTemplate.xcodeproj/project.pbxproj", sys.argv[1], sys.argv[2])
