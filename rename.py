'''
Copyright (C) 2013 Igor Yegorov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

'''

# arg #1 - new project name
# arg #2 - new package name
# arg #3 - old project name
# arg #4 - old package name

import os, sys, shutil

if os.name == "nt":
    print "Doesn't work properly on Windows, please run this script on Mac OS X :("
    sys.exit(0)
	
if not (len(sys.argv) == 5 or len(sys.argv) == 3):
    print "Usage: arg #1 - new project name"
    print "       arg #2 - new package name"
    print "       arg #3 - old project name (or default if not specified)"
    print "       arg #4 - old package name (or default if not specified)"
    print
    print "Default project name: GameTemplate"
    print "Default package name: com.iyegoroff.template"
    print
    sys.exit(0)
	
def fixed_path(path):
    return path.replace("/", "\\") if os.name == "nt" else path

cur_dir = os.getcwd()

new_project_name = sys.argv[1]
new_package_name = sys.argv[2]
old_project_name = sys.argv[3] if len(sys.argv) == 5 else "GameTemplate"
old_package_name = sys.argv[4] if len(sys.argv) == 5 else "com.iyegoroff.template"

# remove generated files
dirs_to_remove = [cur_dir + fixed_path("/proj.android/obj"),
                  cur_dir + fixed_path("/proj.android/bin"),
                  cur_dir + fixed_path("/proj.android/gen"),
                  cur_dir + fixed_path("/proj.android/libs"),
                  cur_dir + fixed_path("/proj.android/out"),
                  cur_dir + fixed_path("/proj.blackberry/Device-Debug"),
                  cur_dir + fixed_path("/proj.blackberry/Device-Release"),
                  cur_dir + fixed_path("/proj.blackberry/Device-Coverage"),
                  cur_dir + fixed_path("/proj.blackberry/Device-Profile"),
                  cur_dir + fixed_path("/proj.blackberry/Simulator"),
                  cur_dir + fixed_path("/proj.blackberry/Simulator-Profile"),
                  cur_dir + fixed_path("/proj.blackberry/Simulator-Coverage"),
                  cur_dir + fixed_path("/proj.wp8/ARM"),
                  cur_dir + fixed_path("/proj.wp8/Release"),
                  cur_dir + fixed_path("/proj.wp8/Debug"),
                  cur_dir + fixed_path("/proj.wp8/WP8"),
                  cur_dir + fixed_path("/proj.wp8/Generated Files")]

[shutil.rmtree(dir_obj) for dir_obj in dirs_to_remove if os.path.exists(dir_obj)]

for root, _, files in os.walk(cur_dir):
	files = [os.path.join(root, f) for f in files if f != ".DS_Store" and f != "rename.py"]
	files = [f for f in files if fixed_path("/.hg/") not in f and fixed_path("/.git/") not in f]

	for fname in files:
		print fname
		fl = open(fname, "r+")
		s = fl.read()

		# replace data in files
		new_string = s.replace(old_project_name, new_project_name)
		new_string = new_string.replace(old_package_name, new_package_name)

		fl.seek(0)
		fl.write(new_string)
		fl.truncate()
		fl.close()

    	# rename files
		new_name = os.path.split(fname)
		os.rename(fname, new_name[0] + fixed_path("/") + new_name[1].replace(old_project_name, new_project_name))

# rename xcodeproj folder
os.rename(cur_dir + fixed_path("/proj.ios/") + old_project_name + ".xcodeproj", cur_dir + fixed_path("/proj.ios/") + new_project_name + ".xcodeproj")

# rename android folders
old_folders = old_package_name.split('.')
new_folders = new_package_name.split('.')

old_folders[0] = cur_dir + fixed_path("/proj.android/src/") + old_folders[0]

for i in range(1,3):
	old_folders[i] = old_folders[i-1] + fixed_path("/") + old_folders[i]

for i in range(2,-1,-1):
	if os.path.exists(old_folders[i]):
		os.rename(old_folders[i], os.path.split(old_folders[i])[0] + fixed_path("/") + new_folders[i])

# rename root folder
if os.path.exists(cur_dir):
    try:
	    os.rename(cur_dir, os.path.split(cur_dir)[0] + fixed_path("/") + new_project_name)
    except WindowsError:
        print "Warning: Can't rename root directory, please rename manually"

